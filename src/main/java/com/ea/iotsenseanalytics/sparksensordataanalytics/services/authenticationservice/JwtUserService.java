package com.ea.iotsenseanalytics.sparksensordataanalytics.services.authenticationservice;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if ("ea_sense".equals(username)) {
            return new User("ea_sense", "$2a$10$T0iqtG.cP4rnWlp1/PpYDecAssIuNpvZhvX0tkM.2WmlNk8AAmZWG",
                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }
}