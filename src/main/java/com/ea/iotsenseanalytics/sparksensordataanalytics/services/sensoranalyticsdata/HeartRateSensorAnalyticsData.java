package com.ea.iotsenseanalytics.sparksensordataanalytics.services.sensoranalyticsdata;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructType;

import java.sql.Timestamp;

import static org.apache.spark.sql.functions.lit;
import static org.apache.spark.sql.types.DataTypes.*;

public class HeartRateSensorAnalyticsData implements SensorAnalyticsData{
    StructType mapperSchema = new StructType()
            .add("id", StringType)
            .add("readTime", LongType)
            .add("unit", StringType)
            .add("reading", FloatType)
            .add("loc", StringType)
            .add("batteryPct", StringType)
            .add("category", StringType);

    Dataset<Row> mappedDF;

    @Override
    public void extractDataframeFromStream(Dataset<Row> rawStreamsData) {
        /**
         * Due to library errors encountered, the spark based calculations
         * will not be implemented in this PoC
         */
        /*
        //Convert JSON message in Kafka to a DataStream with columns
        mappedDF = rawStreamsData
                .selectExpr("CAST(value AS STRING)")
                .select(functions.from_json(
                        functions.col("value"), mapperSchema).as("event"))
                .select("event.id","event.readTime",
                        "event.unit","event.reading", "event.loc");
                        */
    }

    @Override
    public float getAverage(Timestamp fromTime, Timestamp toTime) {
        return 55.5F;
    }

    @Override
    public float getAverageForSensor(String sensorId, Timestamp fromTime, Timestamp toTime) {
        return 0;
    }

    @Override
    public float getMin(Timestamp fromTime, Timestamp toTime) {
        return 45.0F;
    }

    @Override
    public float getMax(Timestamp fromTime, Timestamp toTime) {
        return 78.8F;
    }

    @Override
    public float getMedian(Timestamp fromTime, Timestamp toTime) {
        return 60.3F;
    }
}
