package com.ea.iotsenseanalytics.sparksensordataanalytics.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.ForeachWriter;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.stereotype.Service;

@Service
public class StreamDataSparkService {
    /**
     * Due to library errors encountered, the spark based calculations
     * will not be implemented in this PoC
     */

    static SparkSession spark;

    public StreamDataSparkService() throws Exception{
        createSparkSession();
    }

    public static void createSparkSession() throws Exception {

        //Create the Spark Session
         spark = SparkSession
                .builder()
                .master("local[*]")
                .config("spark.driver.host", "127.0.0.1")
                .config("spark.sql.shuffle.partitions", 2)
                .config("spark.default.parallelism", 2)
                .appName("StreamingAnalyticsExample")
                .getOrCreate();

    }

    /**
     * To get the HeartRate data from the kafka topic
     * @return
     */
    public Dataset<Row> getHeartRateStreamData() {

        System.out.println("Spark session created");

        /**
         * Due to library errors this section is commented-out and
         * will not be implemented in this PoC
         */
        /**
        Dataset<Row> df = spark
                .read()
                .format("kafka")
                .option("kafka.bootstrap.servers", "localhost:29092")
                .option("subscribe", "kafka-sensor-heart-rate")
                .option("startingOffsets", "earliest")
                .load();

        df.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)");
        **/

        Dataset<Row> retDS = null;
        return retDS;
    }

    //To get the FuelLevel data from the kafka topic
    public Dataset<Row> getFuelLevelStreamData() { return null; }

    //To get the Thermostat data from the kafka topic
    public Dataset<Row> getThermostatStreamData() { return null;}


    public void showDF(Dataset<Row> df) throws Exception{
        // display values
        df.writeStream().foreach(
                new ForeachWriter<Row>() {

                    @Override
                    public boolean open(long partitionId, long epochId) {
                        return true;        //Open the connection
                    }

                    @Override
                    public void process(Row record) {
                        try {
                            System.out.println("Row :" + new ObjectMapper().writeValueAsString(record));
                        } catch (Exception e){
                            System.out.println("My Row Mapper:" + e);
                        }
                    }

                    @Override
                    public void close(Throwable errorOrNull) {

                    }
                }
        ).start();
    }


}
