package com.ea.iotsenseanalytics.sparksensordataanalytics.services.sensoranalyticsdata;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.sql.Timestamp;

public interface SensorAnalyticsData {

    public void extractDataframeFromStream(Dataset<Row> rawStreamsData);

    public float getAverage(Timestamp fromTime, Timestamp toTime);

    public float getAverageForSensor(String sensorId, Timestamp fromTime, Timestamp toTime);

    public float getMin(Timestamp fromTime, Timestamp toTime);

    public float getMax(Timestamp fromTime, Timestamp toTime);

    public float getMedian(Timestamp fromTime, Timestamp toTime);

}
