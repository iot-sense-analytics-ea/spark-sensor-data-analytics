package com.ea.iotsenseanalytics.sparksensordataanalytics.utils;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class GeneralOps {
    public Timestamp getTimestampFromString(String tsString, String tsFormat){
        DateTimeFormatter formatDateTime = DateTimeFormatter.ofPattern(tsFormat);
        LocalDateTime localDateTime = LocalDateTime.from(formatDateTime.parse(tsString));
        Timestamp ts = Timestamp.valueOf(localDateTime);
        return ts;
    }
}
