package com.ea.iotsenseanalytics.sparksensordataanalytics.controllers;

import com.ea.iotsenseanalytics.sparksensordataanalytics.services.StreamDataSparkService;
import com.ea.iotsenseanalytics.sparksensordataanalytics.services.sensoranalyticsdata.HeartRateSensorAnalyticsData;
import com.ea.iotsenseanalytics.sparksensordataanalytics.utils.GeneralOps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/sensor/analytics")
public class SensorAnalyticRESTController {

    @Autowired
    StreamDataSparkService sparkService;

    @Autowired
    GeneralOps generalOps;

    @RequestMapping(method = RequestMethod.GET, path = {"/get-sensor-summaries"})
    public ResponseEntity<List<Map<String, String>>> sensorSummaries(@RequestParam(value = "from-timestamp") String fromTimestamp,
                                                                     @RequestParam(value = "to-timestamp") String toTimestamp,
                                                                     @RequestParam(value = "timestamp-format") String timestampFormat) throws Exception {
        List<Map<String,String>> responseList = new ArrayList<>();
        Timestamp fromTime = generalOps.getTimestampFromString(fromTimestamp, timestampFormat);
        Timestamp toTime = generalOps.getTimestampFromString(toTimestamp, timestampFormat);

        // Get HeartRate Stats
        HeartRateSensorAnalyticsData heartRateData = new HeartRateSensorAnalyticsData();
        heartRateData.extractDataframeFromStream(sparkService.getHeartRateStreamData());
        Map<String,String> hrsResultsMap = new HashMap<>();
        hrsResultsMap.put("Avg Heart-Rate", Float.toString(heartRateData.getAverage(fromTime, toTime)) );
        hrsResultsMap.put("Min Heart-Rate", Float.toString(heartRateData.getMin(fromTime, toTime)) );
        hrsResultsMap.put("Max Heart-Rate", Float.toString(heartRateData.getMax(fromTime, toTime)) );
        hrsResultsMap.put("Median Heart-Rate", Float.toString(heartRateData.getMedian(fromTime, toTime)) );
        //Add the results to the return map
        responseList.add(hrsResultsMap);

        ResponseEntity<List<Map<String,String>>> resEntity = new ResponseEntity<>(responseList, HttpStatus.OK);

        return resEntity;
    }

}