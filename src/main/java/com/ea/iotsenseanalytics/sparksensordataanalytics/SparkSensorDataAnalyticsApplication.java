package com.ea.iotsenseanalytics.sparksensordataanalytics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparkSensorDataAnalyticsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SparkSensorDataAnalyticsApplication.class, args);
	}

}
