FROM openjdk:15-jdk-alpine
COPY target/spark-sensor-data-analytics-0.0.1.jar spark-sensor-data-analytics-0.0.1.jar
COPY src/main/resources/application.properties application.properties
ENTRYPOINT ["java","-jar","/spark-sensor-data-analytics-0.0.1.jar"]